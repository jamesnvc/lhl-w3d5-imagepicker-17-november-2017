//
//  ViewController.m
//  ImagePickerDemo
//
//  Created by James Cash on 17-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong,nonatomic) NSString *savedImagePath;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSArray* docsDirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docsDir = [docsDirs firstObject];
    self.savedImagePath = [docsDir stringByAppendingPathComponent:@"saved.jpg"];

    [self loadSavedImage];
}

- (void)saveImage
{
    UIImage* img = self.imageView.image;
    if (img == nil) { return; }
    NSData *imgData = UIImageJPEGRepresentation(img, 1.0);
    [imgData writeToFile:self.savedImagePath atomically:YES];
}

- (void)loadSavedImage
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.savedImagePath]) {
        NSData *imgData = [NSData dataWithContentsOfFile:self.savedImagePath];
        UIImage *img = [UIImage imageWithData:imgData];
        self.imageView.image = img;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pickImage:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        // not running on simulator, take a picture
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }

    NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];

    NSLog(@"Picking media of types %@", mediaTypes);
    picker.mediaTypes = mediaTypes;

    picker.delegate = self;

    [self presentViewController:picker animated:YES completion:^{}];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSLog(@"info %@", info);
    self.imageView.image = info[UIImagePickerControllerOriginalImage];
    [self saveImage];
    [self dismissViewControllerAnimated:YES completion:^{}];
}

@end
